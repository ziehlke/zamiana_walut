package pl.sda.zamiana_walut;

public class Rate {
    private String code;
    private double mid;
    private double ask;
    private double bid;

    public String getCode() {
        return code;
    }

    public double getMid() {
        return mid;
    }

    public double getAsk() {
        return ask;
    }

    public double getBid() {
        return bid;
    }

    @Override
    public String toString() {
        return code;
    }
}
