import com.google.gson.Gson;
import pl.sda.zamiana_walut.Rate;
import pl.sda.zamiana_walut.Rates;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.time.LocalDate;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {
    private static final String API_LINK_MID = "http://api.nbp.pl/api/exchangerates/tables/a/";
    private static final String API_LINK_ASKorBID = "http://api.nbp.pl/api/exchangerates/tables/c/";
    private static final String currencies = "USD|EUR|GBP|CHF";


    public static void main(String[] args) {
        String jsonMid = convertLinkToJson(API_LINK_MID);
        String jsonAskBid = convertLinkToJson(API_LINK_ASKorBID);
        String monthAgo = getWorkDayMonthAgo();
        String jsonAskBidMonthAgo = convertLinkToJson(API_LINK_ASKorBID + monthAgo);

        Gson gson = new Gson();
        Rates ratesMid = gson.fromJson(jsonMid, Rates.class);
        Rates ratesAskBid = gson.fromJson(jsonAskBid, Rates.class);
        Rates ratesAskBidMonthAgo = gson.fromJson(jsonAskBidMonthAgo, Rates.class);

        System.out.println("Aktualne średnie kursy walut oraz ich równowartość dla 100 PLN:");
        for (Rate rate : ratesMid.getRates()) {
            if (rate.getCode().matches(currencies)) {
                System.out.println("\t- 1 " + rate.getCode() + " = " + rate.getMid() + " PLN \t- " +
                        String.format("%.2f", 100 / rate.getMid()) + " " + rate.getCode());
            }
        }

        System.out.println("----------------------------------------------------------------------");

        System.out.println("Aktualne kursy kupna walut oraz ich równowartość dla 100 PLN:");
        ratesAskBid.getRates().stream()
                .filter(rate -> rate.getCode().matches(currencies))
                .forEach(
                        rate -> System.out.println("\t- 1 " + rate.getCode() + " = " + rate.getAsk() + " PLN \t- " +
                                String.format("%.2f", 100 / rate.getAsk()) + " " + rate.getCode()
                        ));

        System.out.println("----------------------------------------------------------------------");

        System.out.println("\nSymulacja, gdybys miesiac temu przeznaczyl 100 PLN na zakup waluty i sprzedal ja dzisiaj:");
        Map<String, Double> currenciesAskMontAgo = ratesAskBidMonthAgo.getRates().stream()
                .filter(rate -> rate.getCode().matches(currencies))
                .collect(Collectors.toMap(Rate::getCode, r -> 100 / r.getAsk()));


        for (Rate rate : ratesAskBid.getRates()) {
            if (rate.getCode().matches(currencies)) {

                double currency = currenciesAskMontAgo.get(rate.getCode());
                System.out.print("\t- Kupiles " + String.format("%.2f", currency) + " " + rate.getCode());
                double have = currency * rate.getBid();
                System.out.println(", dzisiaj sprzedajesz je za " + String.format("%.2f", have) + " PLN");

                String s = (have - 100) > 0 ?
                        "\t- GRATULACJE REKINIE BIZNESU! Zarobiles: " + String.format("%.2f", have - 100) + " PLN\n" :
                        "\t- Niestety stracieles: " + String.format("%.2f", -(have - 100)) + " PLN\n";

                System.out.println(s);
            }
        }
    }

    private static String getWorkDayMonthAgo() {
        String monthAgo;
        switch (LocalDate.now().minusMonths(1).getDayOfWeek()) {
            case SATURDAY:
                System.out.println("UWAGA! Miesiac temu byla sobota - podaje kurs z piatku.");
                monthAgo = String.valueOf(LocalDate.now().minusMonths(1).minusDays(1));
                break;
            case SUNDAY:
                System.out.println("UWAGA! Miesiac temu byla niedziela - podaje kurs z piatku.");
                monthAgo = String.valueOf(LocalDate.now().minusMonths(1).minusDays(2));
                break;
            default:
                monthAgo = String.valueOf(LocalDate.now().minusMonths(1));
        }
        return monthAgo;
    }

    private static String convertLinkToJson(String urlString) {
        String json = "";
        try (BufferedReader bufferedReader = new BufferedReader((new InputStreamReader(new URL(urlString).openStream())))) {
            do {
                json += bufferedReader.readLine();
            } while (bufferedReader.readLine() != null);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return json.substring(1, json.length() - 1);
    }
}
